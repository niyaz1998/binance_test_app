import 'dart:async';
import 'dart:convert';

import 'package:test_binance/model/ticker.dart';
import 'package:web_socket_channel/io.dart';

/// service is something that manages data from repositories
/// requests, stores and etc.
/// services more bound to data stored on server or local storage
/// while BLOCs more bound to UI
///
/// such decision allows multiple BLOCs to subscribe for single service which handles all information
class BinanceService {
  static final BinanceService _singleton = BinanceService._internal();

  factory BinanceService() {
    return _singleton;
  }

  BinanceService._internal();

  static const String END_POINT =
      'wss://stream.binance.com:9443/ws/!miniTicker@arr';

  StreamController<List<BinanceTicker>> _eventController =
      StreamController.broadcast();

  Stream<List<BinanceTicker>> get eventsStream => _eventController.stream;

  IOWebSocketChannel? _channel;

  void start() {
    _channel = IOWebSocketChannel.connect(END_POINT);

    _channel!.stream.listen((data) {
      /// parsing JSON
      List l = jsonDecode(data);
      List<BinanceTicker> tickers =
          l.map((model) => BinanceTicker.fromJson(model)).toList();

      _eventController.add(tickers);
    });
  }

  /// this function is not called, since service lives as long as application lives
  void stop() {
    _eventController.close();
  }
}
