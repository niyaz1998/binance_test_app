class BinanceTicker {
  late String name;
  late String close;
  late String open;
  late String quantity;

  /// 24H difference between [open] and [close] int percentage
  double? get diff {
    var o = double.tryParse(open);
    var c = double.tryParse(close);

    if (o == null || c == null) {
      return null;
    }
    return (o - c) / c * 100;
  }

  BinanceTicker({
    required this.name,
    required this.close,
    required this.open,
    required this.quantity,
  });

  BinanceTicker.fromJson(dynamic json) {
    this.name = json["s"];
    this.close = json["c"];
    this.open = json["o"];
    this.quantity = json["q"];
  }
}
