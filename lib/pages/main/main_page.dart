import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_binance/model/ticker.dart';
import 'package:test_binance/pages/main/bloc_tickers.dart';
import 'package:test_binance/widgets/ticker_list_widget.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late TickerCubit _bloc;

  @override
  void initState() {
    _bloc = TickerCubit();
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _searchField(),
      ),
      body: _tickersList(),
    );
  }

  Widget _tickersList() => BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, List<BinanceTicker> state) {
          return ListView.builder(
            itemCount: state.length,
            padding: EdgeInsets.only(left: 12, right: 12),
            itemBuilder: (c, int index) => Container(
              padding: EdgeInsets.only(top: 12),
              child: TickerListWidget(ticker: state[index]),
            ),
          );
        },
      );

  Widget _searchField() => TextField(
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          isDense: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(100)),
          ),
          prefixIcon: Icon(Icons.search, size: 24),
          hintText: 'Search',
        ),
        onChanged: (String text) {
          _bloc.setSearchText(text);
        },
      );
}
