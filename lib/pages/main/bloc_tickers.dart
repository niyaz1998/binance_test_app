import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_binance/model/ticker.dart';
import 'package:test_binance/services/binance_service.dart';

class TickerCubit extends Cubit<List<BinanceTicker>> {
  late StreamSubscription<List<BinanceTicker>> _tickerStream;

  /// I decided to store 100 tickers and refresh them as they arrive
  Map<String, BinanceTicker> _tickers = {};

  String _searchText = '';

  TickerCubit() : super([]) {
    _tickerStream = BinanceService().eventsStream.listen((event) {
      event.forEach((element) {
        if (_tickers.containsKey(element.name)) {
          /// refreshing old tickers
          _tickers[element.name] = element;
        } else if (_tickers.length < 100) {
          /// adding new
          _tickers[element.name] = element;
        }
      });
      _emitWithSearch();
    });
  }

  void setSearchText(String text) {
    _searchText = text;
    _emitWithSearch();
  }

  void _emitWithSearch() {
    // event.sort((t1, t2) => t1.name.compareTo(t2.name));
    /// decided not to sort, since it simply slows application
    /// and [toList] function already returns tickers in some order
    /// not sure what order... but it's good enough
    if (_searchText.isEmpty) {
      emit(_tickers.values.toList());
    } else {
      List<BinanceTicker> result = [];
      _tickers.forEach((key, value) {
        if (key.toUpperCase().contains(_searchText.toUpperCase())) {
          result.add(value);
        }
      });
      emit(result);
    }
  }

  @override
  Future<void> close() {
    _tickerStream.cancel();
    return super.close();
  }
}
