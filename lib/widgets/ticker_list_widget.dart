import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_binance/model/ticker.dart';

class TickerListWidget extends StatelessWidget {
  final BinanceTicker ticker;

  const TickerListWidget({
    Key? key,
    required this.ticker,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var bigStyle = TextStyle(fontSize: 20, color: Colors.white);
    var smallStyle = TextStyle(fontSize: 10, color: Colors.grey);
    var color = (ticker.diff ?? -1) > 0 ? Colors.green : Colors.red;
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ticker.name,
                  overflow: TextOverflow.ellipsis,
                  style: bigStyle,
                ),
                Text(
                  ticker.quantity,
                  overflow: TextOverflow.ellipsis,
                  style: smallStyle,
                ),
              ],
            ),
          ),
          SizedBox(width: 4, height: 4),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ticker.open,
                  overflow: TextOverflow.ellipsis,
                  style: bigStyle.copyWith(color: color),
                ),
                Text(
                  ticker.open,
                  overflow: TextOverflow.ellipsis,
                  style: smallStyle,
                ),
              ],
            ),
          ),
          SizedBox(width: 4, height: 4),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: 4, bottom: 4),
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(2),
              ),
              alignment: Alignment.center,
              child: Text(
                '${ticker.diff == null ? "error" : "${ticker.diff!.toStringAsFixed(2)} %"}',
                overflow: TextOverflow.ellipsis,
                style: bigStyle.copyWith(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
